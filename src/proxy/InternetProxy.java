/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proxy;

import java.util.Calendar;

/**
 *
 * @author aluno.redes
 */
public class InternetProxy implements ISP {

    @Override
    public String getResource(String site, String user) {
        //Printing Log information
        this.logRequest(site);
        if (this.isBlocked(site, user)) {
            return "blocked by proxy: this site is not permitted in this company!";
        } 
        //returning the content by Internet Provider
        return "by Proxy->"+ new Embratel().getResource(site, user);
        
    }
    
    private void logRequest(String site){
        System.out.println(Calendar.getInstance().getTime()+" Request to - "+site);
    }

    private boolean isBlocked(String site, String user) {
        //Proxy Security Policy
        if (user.equals("admin")) {
            return false; 
        } else {
            switch (site) {
            case "www.google.com":
                return false;
            case "www.yahoo.com":
                return false;
            case "www.xvideos.com":
                return true;
            case "www.youtube.com":
                return true;
            case "www.amazon.com":
                return false;
            default:
                return true;
        }
        }
    }
}
