/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proxy;

/**
 *
 * @author aluno.redes
 */
public class Embratel implements ISP{

    @Override
    public String getResource(String site, String user) {
        switch (site) {
            case "www.google.com":
                return "google page";
            case "www.yahoo.com":
                return "yahoo page";
            case "www.amazon.com":
                return "amazon page";
            case "www.youtube.com":
                return "youtube page";
            case "www.xvideos.com":
                return "xvideos page";
            ///others...
            default:
                return "resource Not Found!";
        }
    }
}
