/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proxy;

import java.util.Scanner;

/**
 *
 * @author aluno.redes
 */
public class Proxy {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Browser firefox = new Browser();
        firefox.sendRequest();
        
        
        /*Modifique o exemplo passado em aula para validar o acesso a sites restritos 
        se o usuário tiver o perfil Admin, esta validação será interna no Proxy. Implemente a solução de maneira elegante.*/
    }
    
}
